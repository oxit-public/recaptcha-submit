# recaptcha-submit

Nette form submit s recaptchou



v configu:
```
extensions:
	recaptcha: OXIT\ReCaptcha\DI\SubmitReCaptchaExtension

recaptcha:
	siteKey: ''
	secretKey: ''
```


Vytváření:
`$form->addRecaptchaSubmit('send' , 'Odeslat')->setHtmlAttribute('class', 'btn-recaptcha');`

UPOZORNĚNÍ: class btn-recaptcha musí na inputu být z důvodu následného navázání JS



v @layout.latte se musí includovat soubor /assets/submitRecatchaJs.latte

