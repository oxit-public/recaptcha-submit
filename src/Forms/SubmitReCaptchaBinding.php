<?php

namespace OXIT\ReCaptcha\Forms;

use OXIT\ReCaptcha\ReCaptchaProvider;
use Nette\Forms\Container;


final class SubmitReCaptchaBinding
{

	/**
	 * @param ReCaptchaProvider $provider
	 * @param string $name
	 * @return void
	 */
	public static function bind(ReCaptchaProvider $provider, $name = 'addRecaptchaSubmit')
	{
		// Bind to form container

		Container::extensionMethod($name, function ($container, $name = 'recaptcha', $caption = 'Send', $required = TRUE) use ($provider) {
			$field = new SubmitReCaptchaField($provider,$caption);
			$field->setRequired($required);
			$container[$name] = $field;

			return $container[$name];
		});
	}

}
