<?php

namespace OXIT\ReCaptcha\Forms;

use Nette\Forms\Controls\SubmitButton;
use Nette\Forms\Form;
use Nette\InvalidStateException;
use Nette\Utils\Html;
use OXIT\ReCaptcha\ReCaptchaProvider;


class SubmitReCaptchaField extends SubmitButton
{

	/** @var ReCaptchaProvider */
	private $provider;

	/** @var bool */
	private $configured = FALSE;

	/**
	 * @param ReCaptchaProvider $provider
	 * @param string $caption
	 * @param string $message
	 */
	public function __construct(ReCaptchaProvider $provider, $caption = 'Send', $message = NULL)
	{

		parent::__construct($caption);
		$this->control->type = 'submit';

		$this->provider = $provider;

		$this->control->addClass('btn-recaptcha');

		if ($message !== NULL) {
			$this->setMessage($message);
		}
	}

	/**
	 * Parse code from form data
	 *
	 * @return void
	 */
	public function loadHttpData(): void
	{
		parent::loadHttpData();
		$this->setValue($this->getForm()->getHttpData(Form::DATA_TEXT, ReCaptchaProvider::FORM_PARAMETER));
	}

	/**
	 * @param string $message
	 * @return static
	 */
	public function setMessage($message)
	{
		if ($this->configured === TRUE) {
			throw new InvalidStateException('Please call setMessage() only once or don\'t pass $message over addRecaptchaSubmit()');
		}

		$this->addRule(function ($code) {
			return $this->verify() === TRUE;
		}, $message);

		$this->configured = TRUE;

		return $this;
	}

	public function validate(): void
	{
		parent::validate();

		if (!$this->verify()) {
			if (!isset($_SESSION["recaptcha-token"]) || $_SESSION["recaptcha-token"] != $this->value) {
				unset($_SESSION["recaptcha-token"]);
				setcookie("recaptcha-submit", $this->value, time() - 3600);
				$this->addError('ERROR: Token is not verified');
			}

		} else {
			$_SESSION["recaptcha-token"] = $this->value;
		}
	}

	/**
	 * @return bool
	 */
	public function verify()
	{
		return $this->provider->validateControl($this) === TRUE;
	}

	/**
	 * Create control
	 *
	 * @param string $caption
	 * @return Html
	 */
	public function getControl($caption = NULL): \Nette\Utils\Html
	{
		$el = parent::getControl();
		$el->addAttributes([
			'data-sitekey' => $this->provider->getSiteKey(),
			'data-size' => 'invisible',
		]);

		if (isset($_SESSION["recaptcha-token"])) {
			$el->addAttributes([
				'data-token' => 'true',
			]);
		}

		return $el;
	}

}
