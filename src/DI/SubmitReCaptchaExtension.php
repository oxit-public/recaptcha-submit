<?php

namespace OXIT\ReCaptcha\DI;

use OXIT\ReCaptcha\Forms\SubmitReCaptchaBinding;
use OXIT\ReCaptcha\ReCaptchaProvider;
use Nette\DI\CompilerExtension;
use Nette\PhpGenerator\ClassType;
use Nette\Utils\Validators;

final class SubmitReCaptchaExtension extends CompilerExtension
{

	/** @var array */
	private $defaults = [
		'siteKey' => NULL,
		'secretKey' => NULL,
	];

	/**
	 * Register services
	 *
	 * @return void
	 */
	public function loadConfiguration()
	{
		$config = $this->validateConfig($this->defaults);
		$builder = $this->getContainerBuilder();

		Validators::assertField($config, 'siteKey', 'string');
		Validators::assertField($config, 'secretKey', 'string');

		$builder->addDefinition($this->prefix('provider'))
			->setFactory(ReCaptchaProvider::class, [$config['siteKey'], $config['secretKey']]);
	}

	/**
	 * Decorate initialize method
	 *
	 * @param ClassType $class
	 * @return void
	 */
	public function afterCompile(ClassType $class)
	{
		$method = $class->getMethod('initialize');
		$method->addBody(sprintf('%s::bind($this->getService(?));', SubmitReCaptchaBinding::class), [$this->prefix('provider')]);
	}

}
